//JavaScript Array

//Array Basic Structure

/*SYNTAX

let/const arrayName = [elementA, elementB, ElementC,...,ElementZ];


*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer','Asus','Lenovo', 'Neo', 'Redfox','Gateway','Toshiba','Fujitsu'];
let mixedArr = [12, 'Asus',null,undefined,true];

let myTask = [
	'drink html',
	'eath javascript',
	'inhale css',
	'bake sass'
]

console.log(grades);
console.log(computerBrands[2]);
console.log(computerBrands[7]);



//Reassign array Values
console.log('Array before reassignment');
console.log(myTask);

myTask[0]= 'hello world';

console.log('Array after reassignment');
console.log(myTask);

//Array Methods

//Mutator Methods
// - are functions thate mutate or change an array after they're created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

//push()
// - adds an element in the end of an array and returns the array's length

console.log("Current Array:");
console.log(fruits);
let fruitsLength=fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated Array from push method:');
console.log(fruits);

//pop()
//remove the last element in an array and returns the removed element

let removedFruit =fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:")
console.log(fruits)

//unshift()
//adds one or more elements at the beginning of an array

fruits.unshift('Lime','Banana');
console.log("Mutated array from unshift method:");
console.log(fruits)

//shift()
//removes an element at the beginning of an array and returns the removed element

let anotherFruit = fruits.shift();
console.log(anotherFruit)
console.log("Mutated array from shift method:");
console.log(fruits);

//splice()
//simultaneously removes elements from a specified index number and adds elements

//Syntax: arrayName.splice(startingIndex,deleteCount, elementToBeAdded)

fruits.splice(1,2,'Lime','Cherry');
console.log("Mutated array from splice method:");
console.log(fruits);

//sort()
//rearrange the array elements in alphanumeric order

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

//reverse();
//reverses the order of array elements

fruits.reverse()
console.log("Mutated array from reverse method:");
console.log(fruits);


//NON MUTATOR METHODS
//these are functions that do not modify or change an array after they're created

let countries = ['US', 'PH','CAN','SG','TH','PH','FR','DE'];

//indexOf()
//returns an index number of the first matching element found in an array
//if no match was found, the result will be -1


let firstIndex= countries.indexOf('PH')
console.log("Result of indexOf method: "+ firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: "+ invalidCountry);

//lastIndexOf
//returns an index number of the last matching element found in an array
/* SYNTAX
	arrayName.lastIndexOf(SearchValue);
	arrayName.lastIndexOf(SearchValue, fromIndex);

*/

let lastIndex = countries.lastIndexOf('PH')
console.log("Result of lastIndexOf method: "+ lastIndex);

let lastIndexStart = countries.lastIndexOf('PH',6)
console.log("Result of lastIndexOf method: "+ lastIndexStart);

//slice()
// portions/slices elements from an array and returns a new array
/*SYNTAX

arrayName.slice(startingIndex);
arrayName,slice(startingIndex,endingIndex);

*/

let sliceArrayA = countries.slice(2);
console.log('Result from slice method: ')
console.log(sliceArrayA)


let sliceArrayB = countries.slice(2,4);
console.log('Result from slice method: ')
console.log(sliceArrayB)



//toString()
//returns an array as a string separated by commas

let stringArray = countries.toString();
console.log('Result from toString method: ')
console.log(stringArray	)

//concat
//combines two arrays and returns the combined result
/* SYNTAX

arrayA.concat(arrayB)
arrayA.concat(ElementA)


*/

let tasksArrayA = ['drink html','eat javascript'];
let tasksArrayB = ['inhale css','breate sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ');
console.log(tasks)



let allTasks = tasksArrayA.concat(tasksArrayB,tasksArrayC)
console.log('Result from concat method');
console.log(allTasks);

let combinedTask = tasksArrayA.concat('smell express','throw react')
console.log('Result from concat method');
console.log(combinedTask)

//join()
//returns an array as a string separated by specified separator string

let user=['John','Jane','Joe','Robert']
console.log(user.join())

console.log(user.join(" - "))

//ITERATION METHODS
//loops designed to perform repetitive task on arrays

//foreach()
//similar to a for loop that iterates on each array element
/*syntax

arrayname.forEach(function(indivElement))

*/

allTasks.forEach(function(task){
	console.log(task)
})

let filteredTask = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTask.push(task);
		}
})
	

console.log("result of filtered task");
console.log(filteredTask)




//map()
//iterates on each element and returns a new array with different values depending on the result of the function's operation
//Syntax

/*

let const resultArray = arrayName.map(function(individualElement))

*/


let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
	return number * number;
})

console.log('result of map method')
console.log(numberMap)

//every()
// check if all elements in an array meet the given condition
/*syntax

let/const resultArray = arrayName.every(function(individualElement){
	return condition;
})

*/

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log('result of every method');
console.log(allValid);

//some()
// checks if at least one element

/*
	
	let/const resultArray = arrayName.some(function(individualElement){
		return condition;
	})

*/

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log("result of some method:")
console.log(someValid)

//filter()
// return a new array that contains elements which meets the given condition
/*
SYNTAX

let/const result array = arrayName.filter(function(individualElements){
	return condition;
})

*/

let filterValid = numbers.filter(function(number){
	return (number <3)
})

console.log("result of filtered method")
console.log(filterValid)

let nothingFound = numbers.filter(function(number){
	return (number === 0)
})

console.log("result of filtered method")
console.log(nothingFound)

//includes method

let product = ["mouse", "keyboard", "laptop", "monitor"];
let filteredProducts = product.filter(function(product){
	return product.toLowerCase().includes('a');
})

console.log("result of includes method")
console.log(filteredProducts)


//reduce()
//evaluate elements from left to right and returns/reduces the array into a single value

/*let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
	return expression/operation;
})
*/

let iteration = 0
let  reduceArray = numbers.reduce(function(x,y){
	console.warn('current iteration' + ++iteration)
	console.log('accumulator: ' +x)
	console.log("currentValue:" +y)

	// operation to reduce the array into a single value
	return x+y

})

console.log("result of reduced method: "+reduceArray);

//multidimensional array
//2D array
/*
	-array within an array
	-let arrayName = [] //1D array

	-letArrayName = [[],[]] //2D array

*/

//2x3 2dimensional array

let twoDim = [[2,4,6],[8,10,12]];

console.log(twoDim[1][2]);
console.log(twoDim[0][1]);
console.log(twoDim[1][0]);

let randomName = [["Juan", "Baldo",],["Maria", "Ann"],["Bantay","Whitey"]];






